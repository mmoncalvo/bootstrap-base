var gulp = require('gulp')
var path = require('path')
var sass = require('gulp-sass')
var uglify = require('gulp-uglify')
var rename = require('gulp-rename')
var concat = require('gulp-concat')
var merge = require('merge-stream')
var connect = require('gulp-connect')
//var nunjucks = require('gulp-nunjucks')
var nunjucksRender = require('gulp-nunjucks-render')

const bootstrapDir = './node_modules/bootstrap-sass'
const bootstrapJs = './node_modules/bootstrap-sass/assets/javascripts'
const bootstrapScss = './node_modules/bootstrap-sass/assets/stylesheets'
const bootstrapFonts = './node_modules/bootstrap-sass/assets/fonts'
const publicDir = './public'

gulp.task('templates', function(done) {
    gulp.src('./src/index.html')
    .pipe(nunjucksRender({ path: '.'}))
    .pipe(gulp.dest(publicDir))
    done()
})

gulp.task('examples', function(done) {
    gulp.src('./examples/**/*/*.html')
    .pipe(nunjucksRender({ path: '.'}))
    .pipe(gulp.dest(publicDir + '/examples'))
    done()
})

gulp.task('sass', function (done) {
    gulp.src('./src/sass/overrides/index.scss')
        .pipe(sass({
            errLogToConsole: true,
            includePaths: bootstrapScss
        }))
        .pipe(rename('styles.css'))
        .pipe(gulp.dest(publicDir + '/css'))
    done()
})

gulp.task('copy', function (done) {
    var fonts = gulp.src(bootstrapFonts + '/bootstrap/**/*')
    .pipe(gulp.dest(publicDir + '/fonts/bootstrap'))
    var examples = gulp.src('./examples/**/*.css')
    .pipe(gulp.dest(publicDir + '/examples'))
    var particles = gulp.src('./particles/**/*')
    .pipe(gulp.dest(publicDir + '/particles'))
    var concatScripts = gulp.src([
        './src/js/jquery.js',
        bootstrapJs + '/bootstrap.js',
        './src/js/**/*.js',
        '!./src/js/holder.min.js'
    ])
    .pipe(concat('all.js'))
    .pipe(uglify())
    var copyScripts = gulp.src('./src/js/holder.min.js')
    .pipe(rename('holder.js'))
    .pipe(gulp.dest(publicDir + '/js'))
    merge(fonts, examples, particles, concatScripts, copyScripts)
    done()
})

gulp.task('connect', function (done) {
    connect.server({
        name: 'Connect server',
        root: ['./public/'],
        port: 8080,
        livereload: true
    })
    done()
})
gulp.task('livereload', function (done) {
    gulp.src('./public/**/*')
        .pipe(connect.reload())
    done()
})

gulp.task('watch', function (done) {
    gulp.watch('./src/sass/**/*.scss', gulp.parallel('sass'))
    gulp.watch('./src/**/*' , gulp.parallel('livereload'))
    done()
})

gulp.task('build', gulp.series('templates', 'examples', 'sass', 'copy', function (done) {
    done()
}))

gulp.task('default', gulp.series('build', 'connect', 'watch', function (done) {
    done()
}))